Source: libpuzzle
Section: graphics
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Emmanuel Bouthenot <kolter@debian.org>
Build-Depends:
    debhelper (>= 12),
    libgd-dev
Standards-Version: 4.3.0
Homepage: https://github.com/jedisct1/libpuzzle
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/libpuzzle
Vcs-Git: https://salsa.debian.org/debian-phototools-team/libpuzzle.git

Package: libpuzzle1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: quick similar image finder - shared library
 The Puzzle library is designed to quickly find visually similar images
 (GIF, PNG, JPG), even if they have been resized, recompressed,
 recolored or slightly modified.
 .
 This package contains the C library.

Package: libpuzzle-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: quick similar image finder - runtime tool
 The Puzzle library is designed to quickly find visually similar images
 (GIF, PNG, JPG), even if they have been resized, recompressed,
 recolored or slightly modified.
 .
 This package contains the command-line tool: puzzle-diff.

Package: libpuzzle-dev
Section: libdevel
Architecture: any
Depends: libpuzzle1 (= ${binary:Version}), ${misc:Depends}
Description: quick similar image finder - development files
 The Puzzle library is designed to quickly find visually similar images
 (GIF, PNG, JPG), even if they have been resized, recompressed,
 recolored or slightly modified.
 .
 This package contains the development files.
